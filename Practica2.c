#include <stdio.h>
#include <stdlib.h>
/*Se crean las distintas funciones para
nuestras operaciones que se desean realizar
y utilizremos en nuestra funcion principal
que es el menu de nuestro programa
En esta parte se crea la funcion suma*/
void suma ()
{
    /*Se declaran las varibles de almacenamiento para tres datos tipo flotante*/
    float valor1,valor2,valor3;
    /*se imprimen las instrucciones para el usuario*/
    printf("Ingresa los valores que quieres sumar");
    printf("\nNota tu puedes ingresar tambien puntos decimales pero en el resultado solo apareceran dos decimales");
    /*Guardamos los datos escritos por el usuario para su futuro uso*/
    printf("\nIngrese el primer valor:");
    scanf("%f",&valor1);
    printf("Ingrese el segundo valor:");
    scanf("%f",&valor2);
    /*Se hace la operacion seleccionada por el usuario con los datos que ingreso
     y se aguarda el resultado en un tercer dato*/
    valor3=valor1+valor2;
    /*Se imprime el resultado del usuario y se termina la funcion seleccionada*/
    printf("El resultado de tu suma es:%.2f",valor3);
}
/*En esta parte se crea la funcion resta*/
void resta ()
{
    /*Se declaran las varibles de almacenamiento para tres datos tipo flotante*/
    float valor1,valor2,valor3;
    /*se imprimen las instrucciones para el usuario*/
    printf("Ingresa los valores que quieres restar");
    printf("\nNota tu puedes ingresar tambien puntos decimales pero en el resultado solo apareceran dos decimales");
    /*Guardamos los datos escritos por el usuario para su futuro uso*/
    printf("\nIngrese el primer valor:");
    scanf("%f",&valor1);
    printf("Ingrese el segundo valor:");
    scanf("%f",&valor2);
    /*Se hace la operacion seleccionada por el usuario con los datos que ingreso
    y se aguarda el resultado en un tercer dato*/
    valor3=valor1-valor2;
    /*Se imprime el resultado del usuario y se termina la funcion seleccionada*/
    printf("El resultado de tu resta es:%.2f",valor3);
}
/*En esta parte se crea la funcion multiplicar*/
void multiplicacion ()
{
    /*Se declaran las varibles de almacenamiento para tres datos tipo flotante*/
    float valor1,valor2,valor3;
    /*se imprimen las instrucciones para el usuario*/
    printf("Ingresa los valores que quieres multiplicar");
    printf("\nNota tu puedes ingresar tambien puntos decimales pero en el resultado solo apareceran dos decimales");
    /*Guardamos los datos escritos por el usuario para su futuro uso*/
    printf("\nIngrese el primer valor:");
    scanf("%f",&valor1);
    printf("Ingrese el segundo valor:");
    scanf("%f",&valor2);
    /*Se hace la operacion seleccionada por el usuario con los datos que ingreso
    y se aguarda el resultado en un tercer dato*/
    valor3=valor1*valor2;
    /*Se imprime el resultado del usuario y se termina la funcion seleccionada*/
    printf("El resultado de tu multiplicacion es:%.2f",valor3);
}
/*En esta parte se hace la operacion de la division*/
void division ()
{
    /*Se declaran las varibles de almacenamiento para tres datos tipo flotante*/
    float valor1,valor2,valor3;
    /*se imprimen las instrucciones para el usuario*/
    printf("Ingresa los valores que quieres dividir");
    printf("\nNota tu puedes ingresar tambien puntos decimales pero en el resultado solo apareceran dos decimales");
    /*Guardamos los datos escritos por el usuario para su futuro uso*/
    printf("\nIngrese el primer valor:");
    scanf("%f",&valor1);
    printf("Ingrese el segundo valor:");
    scanf("%f",&valor2);
    /*Se hace la operacion seleccionada por el usuario con los datos que ingreso
    y se aguarda el resultado en un tercer dato*/
    valor3=valor1*valor2;
    /*Se imprime el resultado del usuario y se termina la funcion seleccionada*/
    printf("El resultado de tu division es:%.2f",valor3);
}
/*En esta parte se hace la operacion de residuo*/
void residuo ()
{
    /*Se declaran las varibles de almacenamiento para tres datos tipo entero*/
    int valor1,valor2,valor3;
    /*se imprimen las instrucciones para el usuario*/
    printf("Ingresa los valores de la division del cual quieres saber el residuo");
    printf("\nNota solo puedes ingresar numeros enteros");
    printf("\nIngrese el primer valor:");
    /*Guardamos los datos escritos por el usuario para su futuro uso*/
    scanf("%d",&valor1);
    printf("Ingrese el segundo valor:");
    scanf("%d",&valor2);
    /*Se hace la operacion seleccionada por el usuario con los datos que ingreso
    y se aguarda el resultado en un tercer dato*/
    valor3=valor1%valor2;
    /*Se imprime el resultado del usuario y se termina la funcion seleccionada*/
    printf("El residuo de tu division es:%d",valor3);
}
/*En esta funcion hacemos un
 menu para seleccionar el operador racional
 que se quiere utilizarrealizar utilizando
 la sentencia switch parecido al menu de nuestra
 funcion principal*/
void operadoresracionales ()
{
    /*Se declaran tres variables de almacenamiento de tipo entero*/
    int opcion,valor1,valor2;
    /*Se imprime el meno de operadores racionales*/
    printf("\nOperadores racionales");
    printf("\nOpciones:\n1.Mayor que\n2.Menor que\n3.Mayor o igual que\n4.Menor o igual que\n5.Distinto\n6.igual\n7.Regresar");
    printf("\nOpcion que quieres utilizar:");
    /*Guardamos el dato escrito por el usuario para su futuro uso
     en la sentencia switch*/
    scanf("%d",&opcion);
    /*Se crea la sentencia switch para realizar el
    operador deseado por el usuario*/
    switch(opcion)
    {
        /*se crea lo que se va a hacer dependiendo de
        que operador racional haya elegido el usuario
        guardando sus datos ingresados que le pedira
        cada caso y se imprimira una afirmacion dependiendo
        de los datos ingresados esto utilizando las
        sentencias if y else y te volvera a imprimir el
        menu de operadores racionales esto debido al break
        */
        case 1:{printf("Ingresa dos valores enteros:");
                printf("\nValor 1:");
                scanf("%d",&valor1);
                printf("Valor 2:");
                scanf("%d",&valor2);
                if(valor1>valor2){printf("Tu valor 1 es mayor que el valor 2");}
                else{printf("Tu valor 1 no es mayor que el valor 2");}break;}
        case 2:{printf("Ingresa dos valores enteros:");
                printf("\nValor 1:");
                scanf("%d",&valor1);
                printf("Valor 2:");
                scanf("%d",&valor2);
                if(valor1<valor2){printf("Tu valor 1 es menor que el valor 2");}
                else{printf("Tu valor 1 no es menor que el valor 2");}break;}
        case 3:{printf("Ingresa dos valores enteros:");
                printf("\nValor 1:");
                scanf("%d",&valor1);
                printf("Valor 2:");
                scanf("%d",&valor2);
                if(valor1>=valor2){printf("Tu valor 1 es mayor o igual que el valor 2");}
                else{printf("Tu valor 1 no es mayor ni igual que el valor 2");}break;}
         case 4:{printf("Ingresa dos valores enteros:");
                printf("\nValor 1:");
                scanf("%d",&valor1);
                printf("Valor 2:");
                scanf("%d",&valor2);
                if(valor1<=valor2){printf("Tu valor 1 es menor o igual que el valor 2");}
                else{printf("Tu valor 1 no es menor ni igual que el valor 2");}break;}
         case 5:{printf("Ingresa dos valores enteros:");
                printf("\nValor 1:");
                scanf("%d",&valor1);
                printf("Valor 2:");
                scanf("%d",&valor2);
                if(valor1!=valor2){printf("Tu valor 1 es distinto al valor 2");}
                else{printf("Tu valor 1 es igual al valor 2");}break;}
         case 6:{printf("Ingresa dos valores enteros:");
                printf("\nValor 1:");
                scanf("%d",&valor1);
                printf("Valor 2:");
                scanf("%d",&valor2);
                if(valor1==valor2){printf("Tu valor 1 es igual que el valor 2");}
                else{printf("Tu valor 1 es dinstinto al valor 2");}break;}
         /*En este caso se realizo una intruccion para regresar
         al anterior menu*/
         case 7:{break;}
         /*En caso de tener un numero distinto a el de los casos
         se imprimira opcion no valida y tambien el menu de
         operadore racionales*/
         default:{printf("Opcion no valida");operadoresracionales();}

    }
}
/*Funcion principal aqui se encuentra la estructura
inicial de nuestro programa que esl basicamente un menu
de los operadores basicos de c y se llamara a invocar
las funciones anteriormente echas*/
int main()
{
    /*se declara la varible entera que utilizaremos mas adelante
    en la sentencia switch*/
    int opcion;
   /*Se le indica al usuario las intrucciones del menu*/
    printf("\nBienvenido a tu programa de operadores");
    printf("\n1.Suma\n2.Resta\n3.Multiplicacion\n4.Division\n5.Residuo\n6.Operadores racionales\n7.Salir del programa");
    printf("\nSelecciona la operacion deseada:");
   /*se guarda el dato del entero*/
    scanf("%d",&opcion);
    /*Se usa la sentencia switch para poder acceder a las diferentes funciones
    que hicimos anteriormente al termino de la funcion solicitada te regresara
    al inicio de nuestra funcion principal ya que en cada uno de los casos
    se le pide invocar la funcion principal despues de la funcion de la operacion
    que el usuario quiere hacer excepto del caso 7 ya que se le a�adio una opcion
    de salida del programa*/
    switch(opcion)
    {
        case 1:{suma();main();}
        case 2:{resta();main();}
        case 3:{multiplicacion();main();}
        case 4:{division();main();}
        case 5:{residuo();main();}
        case 6:{operadoresracionales();main();}
        case 7:{printf("�Adios!");exit(EXIT_FAILURE);}
        /*En caso de no ingresar un numero que solicito el menu se imprimira
        en pantalla opcion no valida y aparecera de nuevo el menu principal
        esto debido a que esto hace el default se podria decir que se
        creo un ciclo*/
        default:{printf("Opcion no valida");main();}
    }
    /*Terminamos el programa con el estado "finalizado normalmente"*/
    return 0;
}
